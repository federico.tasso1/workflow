
![flow](https://gitlab.com/federico.tasso1/workflow/-/raw/main/gitflowExample.png)

### The gist

1. `master` must always be deployable.
1. **all changes** made through feature branches (pull-request + merge)
1. rebase to avoid/resolve conflicts; merge in to `master`

### The workflow

```bash
# everything is happy and up-to-date in master
git checkout master
git pull origin master

# let's branch to make changes
git checkout -b my-new-feature

# go ahead, make changes now.
$EDITOR file

# commit your (incremental, atomic) changes
git add -p
git commit -m "my changes"

# keep abreast of other changes, to your feature branch or master.
# rebasing keeps our code working, merging easy, and history clean.
git pull --rebase origin:master

# push your branch for discussion (pull-request)
# you might do this many times as you develop.
git push -f origin my-new-feature

```
### Merge when done developing:

* Use gitLab interface to crerate a new merge request:  feature/fix branch => master
* Add a little description of what have you done 
* Assign the merge request to a colleague

  [ click here for more hints ](https://mtlynch.io/code-review-love/)




### Tag important things, such as releases
Once is all finish , use the gitLab interface to create a tag release
with a helpfull name.
### DOs and DON'Ts

No DO or DON'T is sacred. You'll obviously run into exceptions, and develop 
your own way of doing things. However, these are guidelines I've found
useful.

#### DOs

- **DO** keep `master` in working order.
- **DO** rebase your feature branches.
  - **DO** pull in (rebase on top of) changes
- **DO** tag releases
- **DO** push feature branches for discussion
- **DO** learn to rebase


#### DON'Ts

- **DON'T** merge in broken code.
- **DON'T** commit onto `master` directly.
  - **DON'T** hotfix onto master! use a feature branch.
- **DON'T** rebase `master`.
- **DON'T** merge with conflicts. handle conflicts upon rebasing.
